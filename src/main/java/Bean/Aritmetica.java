package Bean;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.JOptionPane;

@ManagedBean
@RequestScoped
public class Aritmetica extends HttpServlet implements Serializable{

	private String n1;
	private String n2;
	private String operacion;
	
	
	
	public String getN1() {
		return n1;
	}


	public void setN1(String n1) {
		this.n1 = n1;
	}


	public String getN2() {
		return n2;
	}


	public void setN2(String n2) {
		this.n2 = n2;
	}


	public String getOperacion() {
		return operacion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public static boolean esUnNumero(String numero) {

        boolean resultado;

        try {
            Integer.parseInt(numero);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            resultado = false;
        }

        return resultado;
    }
	
		
	public String aritmetica() {
	
		int numero1;
		int numero2;
		
		HttpServletRequest req = (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		
		operacion = req.getParameter("operacion");
		n1 = req.getParameter("numero1");
		n2 = req.getParameter("numero2");
		
		if(n1 == null || n2 == null) {

			System.out.println("Faltan parametros");
			return "Faltan parametros";
		}else if(operacion == null) {
			System.out.println("Operacion no especificada");
			return "operacion no especificada";
		}else if(esUnNumero(n1) == false || esUnNumero(n2) == false) {
			System.out.println("los parametros tienen valores no soportados");
			return "Los parametros contienen valores no soportados";
		}else if(operacion.equals("suma")){
			numero1 = Integer.parseInt(n1);
			numero2 = Integer.parseInt(n2);
			System.out.println("La suma es igual a: "+(numero1+numero2));
			return "La suma es igual a: "+(numero1+numero2);
		}else {
			System.out.println("operacion no soportada");
			return "operacion no soportada lo sentimos";
		}
		
	}
	
}
